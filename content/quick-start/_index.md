---
title: "Get Started"
subtitle: "For now, the easiest to do it, is using [scoop](https://scoop.sh/ \"A command-line installer for Windows\").</br>You can do it in only 3 steps !"
# meta description
description: ""
draft: false
---

1. Open a [PowerShell](https://www.howtogeek.com/662611/9-ways-to-open-powershell-in-windows-10/ "9 Ways to Open PowerShell in Windows 10").
2. Copy the following lines :  
    By adding the bucket (recommended to get updates with `scoop update *`) :  
    ```ps
    scoop bucket add chacas0 https://gitlab.com/ChacaS0/scoop-bucket
    ```
    ```ps
    scoop install keepdrivealivegui
    ```
      
    Or as a one-timer :
    ```ps
    scoop install https://gitlab.com/keepdrivealive/keepdrivealivegui/-/raw/master/keepdrivealivegui.json
    ```
3. Right click into the PowerShell window to paste this and then hit `Enter` .



----------------------------------------

</br>

### Build from source

If you want to build from source, check the [**Documentation**](/documentation).
