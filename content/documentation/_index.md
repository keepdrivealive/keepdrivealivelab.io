---
title: "Documenation"
subtitle: ""
# meta description
description: "Build KeepDriveAlive from source. Use Golang, Go, rc, Directory tree, Tree, Keep Drive Alive, Keep Drive Spinning"
draft: false
---

## Requirements
You will need the following ready to be used :

* Internet
* Golang
* Git
* PowerShell
* Know your way around the command line


## Choose where to install
Choose where you are going to install `KeepDriveAlive`. The place you chose will be called `$dir` in this document.

You need to create first the `KeepDriveAlive` directory in `$dir`.

```powershell
mkdir KeepDriveAlive
cd KeepDriveAlive
```

## KeepDriveAliveService
1. We install the service like so :
    ```powershell
    git clone https://gitlab.com/keepdrivealive/keepdrivealiveservice.git
    cd keepdrivealiveservice
    go build -o keepDriveAlive.exe
    ```
2. Now with administrator/elevated privileges run the following :
    ```powershell
    .\keepDriveAlive.exe install
    ```
3. Go back to the original `$dir\KeepDriveAlive` directory.
    ```powershell
    cd ..
    ```

### Note
The other commands to `keepDriveAlive.exe` are :

* `.\keepDriveAlive.exe start`
* `.\keepDriveAlive.exe stop`
* `.\keepDriveAlive.exe remove`

## KeepDriveAliveGUI (optional)
If you want a graphical interface to help you manage the background service, follow these steps in order to install KeepDriveAliveGUI, if not, you're done.

1. Download the source :
    ```powershell
    git clone https://gitlab.com/keepdrivealive/keepdrivealivegui.git
    cd keepdrivealivegui
    ```
2. Build :
    ```powershell
    go build -ldflags "-extldflags '-static' -H windowsgui" -o KeepdriveAliveGUI.exe
    ```

## File structure
You should end up with a structure like this :
```Unicode
KeepDriveAlive
├── keepdrivealivegui
│   ├── .gitignore
│   ├── go.mod
│   ├── go.sum
│   ├── keepDriveAlive.ico
│   ├── keepDriveAlive_transparent.png
│   ├── keepdrivealivegui.exe
│   ├── keepDriveAliveGUI.exe.manifest
│   └── LICENSE
│   └── main.go
└── keepdrivealiveservice
    ├── .gitignore
    ├── go.mod
    ├── install.go
    ├── keepDriveAlive.drawio
    ├── keepDriveAlive.exe
    ├── keepDriveAlive.exe.conf
    ├── keepDriveAlive.exe.conf.example
    ├── keepDriveAlive.go
    ├── keepDriveAlive.png
    ├── keepDriveAlive_transparent.png
    ├── keepDriveAliveService.exe
    ├── main.go
    ├── manage.go
    ├── README.md
    └── service.go
```

## Access
### Shortcut
You can create [*create a shortcut*](https://youtu.be/Un5DXGJO1uM "Youtube - How to Make Desktop Shortcuts - Windows 10 Tutorial") of `$dir\KeepDriveAlive\keepdrivealivegui\keepdrivealivegui.exe` and put it on your Desktop for example.

### PATH
If you intend to use directly the `KeepDriveAliveService`, it might be handy to add `$dir\KeepDriveAlive\keepdrivealiveservice\keepDriveAlive.exe` [*to your PATH*](https://youtu.be/a2igMMI7iiE "Youtube - Setup folder in a `PATH` variable on Windows").

This will be mostly used if you didn't install the KeepDriveAliveGUI.
